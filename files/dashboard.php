<?php
    include "conn.php";
    session_start();
    $id = $_SESSION['id'];
    if($id == 0)
    {
        header("Location: error.php");
    }
    $sql = "SELECT * FROM users WHERE id='$id'";
    if($res = mysqli_query($conn,$sql))
	{
		if(mysqli_num_rows($res)>0)
		{
			while($row=mysqli_fetch_array($res))
			{
                $name = $row['fname'] ." ". $row['mname']. " ". $row['lname'];
                $email = $row['email'];
                $mobile = $row['mobile'];
			}
        }
    }
    
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <?php
        include 'link.php';
    ?>
    <link rel="stylesheet" type="text/css" href="../css/dashboard.css">
    <!-- <script src="../js/dashboard.js"></script> -->
</head>
<body>
    <div id="throbber" style="display:none; min-height:120px;"></div>
    <div id="noty-holder"></div>

    <div id="wrapper">

        <!-- Navigation -->
      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <i class="fa fa-bars" aria-hidden="true" style="color: white;"></i>
                </button>
                <!-- <div class="navbar-brand">   
                </div> -->
            </div>

            <!-- <button class="btn  search-btn-icon"> -->
             <!-- <i class="fa fa-search" aria-hidden="true" style="color:white"></i>  
             <input type="Search" placeholder="Search..." class="form-control-serch search-box" />                  -->
            <!-- </button> -->
                  
            <!-- Top Menu Items -->  
            <?php
                echo '<h3 style="color:white;">'.$name.'</h3>';
            ?>   
               
            <div class="items">
              <ul class="nav top-nav">  
                <li class="editpro navbar-right">
                  <i class="fasett fa-cog" aria-hidden="true" class="menu-button" id="menu-button"></i> 
                  <h5 class="pull-left login-person-head">Welcome WaLiA SaAB</h5> 
                </li>
              </ul>
            </div>

            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse" style="background-color: #616060; border:1px solid #616060;">
                <ul class="nav navbar-nav side-nav">
                  <a href="#"><img class="logostyle" src="https://vignette.wikia.nocookie.net/nationstates/images/2/29/WS_Logo.png/revision/latest?cb=20080507063620" alt="LOGO""></a>
                    <li>
                       <a class="active" href="#" data-toggle="collapse" data-target="#submenu-1"><i class="fa fa-home" aria-hidden="true"></i>   <span style="color:white;">  Home </span></a>
                    </li>
                    <li>
                        <a class="#" href="#" data-toggle="collapse" data-target="#submenu-1"><i class="fa fa-user-o" aria-hidden="true"></i>   <span style="color:white;">  Profile </span></a>
                    </li>
                    <li>
                        <a class="#" href="#" data-toggle="collapse" data-target="#submenu-1"><i class="fa fa-calendar" aria-hidden="true"></i>   <span style="color:white;"> Events </span></a>
                    </li>
                    <li>
                        <a class="#" href="#" data-toggle="collapse" data-target="#submenu-1"><i class="fa fa-envelope" aria-hidden="true"></i>  <span style="color:white;"> Messages </span></a>
                    </li>
                    <li>
                        <a class="#" href="#" data-toggle="collapse" data-target="#submenu-1"><i class="fa fa-cogs" aria-hidden="true"></i>   <span style="color:white;"> Settings </span></a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
      </nav>


    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 dashhead">
                <h1>Profile</h1>
                <?php
                    echo '<h3>Name : '.$name.'</h3>';
                    echo '<h3>Email : '.$email.'</h3>';
                    echo '<h3>Mobile : '.$mobile.'</h3>';
                ?>
            </div>
        </div>
    </div>

    </div><!-- /#wrapper -->
</body>
</html>