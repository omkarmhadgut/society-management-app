<?php
    include "conn.php";
	session_start();
    if(isset($_POST['submit']))
    {
        $uid = $_POST['uid'];
        $pass = $_POST['pass'];

        $sql = "SELECT * FROM users WHERE  uid='$uid' AND pass='$pass'";
        if($res = mysqli_query($conn,$sql))
		{
			if(mysqli_num_rows($res)>0)
	    	{
	    		while($row=mysqli_fetch_array($res))
	    		{
					$id=$row['id'];
					$_SESSION['id'] =$id; 
					if($_SESSION['id'])
					{
						header("Location: dashboard.php");
                    }
	    		}
			}
			else
					{
						echo '<script>alert("Enter valid details.")</script>';
					}
        }
        else
		{
			echo '<script>alert("Enter valid details.")</script>';
		}
    }

?>


<!DOCTYPE html>
<html>
<head>
	<title>Login Page</title>
   <!--Made with love by Mutiullah Samim -->
   <?php
   include 'link.php';
   ?>
	<!--Bootsrap 4 CDN-->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    
    <!--Fontawesome CDN-->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

	<!--Custom styles-->
	<link rel="stylesheet" type="text/css" href="../css/login.css">
</head>
<body>
<div class="container">

	<div class="d-flex justify-content h-100">
	<div class="col-md-4"></div>
<div class="col-md-4"></div>
<div class="col-md-4" style="margin-top:140px">
		<div class="card">
			<div class="card-header asd">
				<center><h3 style='margin:7px'>Sign In</h3></center>
			</div>
			<div class="card-body">
				<form action="login.php" method="POST">
                    <br><br>
					<div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-user"></i></span>
                        </div>
                        <input type="text" name="uid" class="form-control" placeholder="Username">
                </div>
                    <br><br>
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><center><i class="fas fa-key"></i></center></span>
						</div>
						<input type="password" class="form-control" placeholder="password" id="user" name="pass">
					</div><br><br>
					<div class="form-group">
						<center><input type="submit" value="Login" class="btn login_btn" name="submit"></center>
					</div>
				</form>
			</div>
			<div class="card-footer">
				<div class="d-flex justify-content-center links">
					Don't have an account?<a href="sign-up.php">Sign Up</a>
				</div>
				<div class="d-flex justify-content-center">
					<a href="forgot.php">Forgot your password?</a>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</body>
</html>