<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign-up</title>
    <?php
   include 'link.php';
   ?>
	<!--Bootsrap 4 CDN-->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    
    <!--Fontawesome CDN-->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

	<!--Custom styles-->
	<link rel="stylesheet" type="text/css" href="../css/sign-up.css">
</head>
<body>
    <div class="container-fluid" >
        
        
        <div class="col-md-2">
            
        </div>
    	<div class="col-md-8 well asd1" style="border-top: 5px solid rgba(235, 231, 231, 0.952);">
    	<div class="">
            <center><h1 class="asd">Registration Form</h1></center>
    				<form>
    					<div class="col-md-12">
    						<div class="row">
    							<div class="col-md-6 form-group">
    								<label>First Name : </label>
    								<input type="text" placeholder="Enter First Name Here.." class="form-control">
                                </div>
                                <div class="col-md-6 form-group">
    								<label>Middle Name :</label>
    								<input type="text" placeholder="Enter First Name Here.." class="form-control">
    							</div>
    						</div>					
    							
    						<div class="row">
                                <div class="col-md-6 form-group">
                                    <label>Last Name : </label>
                                    <input type="text" placeholder="Enter Last Name Here.." class="form-control">
                                </div>
                                <div class="col-md-6 form-group">
                                        <label>Email-id : </label>
                                        <input type="text" placeholder="Enter Email-id Here.." class="form-control">
                                </div>
    								
                            </div>
                            <!-- <div class="form-group">
                                <label>Address</label>
    							<textarea placeholder="Enter Address Here.." rows="3" class="form-control"></textarea>
                            </div> -->
                            <div class="row" style="margin-top:10px;">
                                <div class="col-md-6 form-group">
    								<label for="section">Society Section : &nbsp;</label><br>
                                    <!-- <input type="text" placeholder="Enter City Name Here.." class="form-control"> -->
                                    <select id="section" style="font-size: 16px !important;height:50px;width:200px !important;">
                                      <option value="A" selected>A Section</option>
                                      <option value="B">B Section</option>
                                      <option value="C">C Section</option>
                                      <option value="D">D Section</option>
                                    </select>
    							</div>
                                <div class="col-md-6 form-group">
                                    <label for="floor">Floor : </label><br>
                                    <!-- <input type="text" placeholder="Enter State Name Here.." class="form-control"> -->
                                    <select id="floor" style="font-size: 16px !important;height:50px;width:200px !important;">
                                      <option value="1" selected>1st Floor</option>
                                      <option value="2">2nd Floor</option>
                                      <option value="3">3rd Floor</option>
                                      <option value="4">4th Floor</option>
                                    </select>
                                </div>
                            </div>
    						
                            <div class="row">	
                                <div class="col-md-6 form-group">
    					        	<label>Register No. :</label>
    					        	<input type="text" placeholder="Enter Register No. Here.." class="form-control">
    					        </div>					
                                <div class="col-md-6 form-group">
                                    <label>Mobile No :</label>
                                    <input type="text" placeholder="Enter Mibile No. Here.." class="form-control">
                                </div>		
                            </div>        	
    						<div class="row">
                                <div class="col-md-6 form-group">
    								<label>Room No. :</label>
    								<input type="text" placeholder="Enter Room No. Here.." class="form-control">
    							</div>
                                <div class="col-md-4 form-group">
                                    <label>No. Of Family Members</label>
                                    <input type="text" placeholder="Enter No. of Family Members Here.." class="form-control">
                                </div>
                            </div>	
    					<!-- <div class="form-group">
    						<label>Email Address</label>
    						<input type="text" placeholder="Enter Email Address Here.." class="form-control">
    					</div>	
    					<div class="form-group">
    						<label>Website</label>
    						<input type="text" placeholder="Enter Website Name Here.." class="form-control">
                        </div> -->
                        <div class="col-md-3">

                        </div>
    					<input type="submit" class="btn btn-lg btn-info" value="Submit">				
    					</div>
    				</form> 
    				</div>
    	</div>
    </div>
</body>
</html>